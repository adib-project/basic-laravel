<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class AdminLoginController extends Controller
{
	public function __costruct(){
		$this->middleware('guest:admin');
	}

    public  function showLoginForm(){
    	return view('auth.admin-login');
    }

    public function login(Request $request){
    	//validasi data
    	$this->validate($request, [
    		'email' => 'required|email',
    		'password' => 'required|min:6'
    	]);

    	//attemp to log the user in
    	if(Auth::guard('admin')->attempt(['email' => $request->email, 'password'=>$request->password], $request->remember)){
    		//if success
    		return redirect()->intended(route('admin.dashboard'));
    	}

    	//ifunsuccess
    	return redirect()->back()->withInput($request->only('email', 'remember'));
    }
}
